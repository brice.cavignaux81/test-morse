package tests;

import com.company.MorseCode;
import com.company.MorseCodeDecoder;
import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class morseTest {

    @Test
    public void testMorseCode() {
        assertThat(MorseCode.get("...."), is("H"));
        assertThat(MorseCode.get(".---"), is("J"));
    }

    @Test
    public void testMorseCodeDecoder() {
        assertThat(MorseCodeDecoder.decodeMorse(".... . -.--   .--- ..- -.. ."), is("HEY JUDE"));
    }

    @Test
    public void testDecodeBits() throws Exception {
        assertThat(MorseCodeDecoder.decodeMorse(MorseCodeDecoder.decodeBits("1100110011001100000011000000111111001100111111001111110000000000000011001111110011111100111111000000110011001111110000001111110011001100000011")), is("HEY JUDE"));
    }
}
